import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable, BehaviorSubject, from } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-nav-header',
  templateUrl: './nav-header.component.html',
  styleUrls: ['./nav-header.component.scss']
})
export class NavHeaderComponent {

  // isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  //   .pipe(
  //     map(result => result.matches)
  //   );

  private handSetSubject = new BehaviorSubject<boolean>(true);
  isHandset$ = from(this.handSetSubject);

  constructor(private breakpointObserver: BreakpointObserver) {}

}
