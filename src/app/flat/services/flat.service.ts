import { Flat } from './../flat.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FlatService {

  constructor(private _http: HttpClient) {
  }

  fetchFlatData() {
    console.log('fetch');
    return this._http.get(
      'http://localhost:8080/flats?pagination=false'
    ).pipe(
      map((flatsHydra) => {
        const flats = flatsHydra['hydra:member'];
        console.log(flats);
        return flats;
      })
    );
  }
}
