import { FlatPageQuery } from './../ngrx/flat.actions';
import { BehaviorSubject, Observable } from 'rxjs';
import { Flat } from '../flat.model';
import { DataSource, CollectionViewer } from '@angular/cdk/collections';
import { Store } from '@ngrx/store';
import { AppState } from '../../reducers';


export class FlatDataSource implements DataSource<Flat> {

  private _flatSubject = new BehaviorSubject<Flat[]>([]);

  constructor(private _store: Store<AppState>) {}

  loadFlat(page: FlatPageQuery) {
    this._store.pipe(

    );
  }

  connect(collectionViewer: CollectionViewer): Observable<Flat[]> {
    return this._flatSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer) {
    this._flatSubject.complete();
  }

}
