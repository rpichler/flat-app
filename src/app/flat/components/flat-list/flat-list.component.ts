import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Flat } from './../../flat.model';

@Component({
  selector: 'app-flat-list',
  templateUrl: './flat-list.component.html',
  styleUrls: ['./flat-list.component.scss']
})
export class FlatListComponent implements OnInit {

  @Input() flats: Flat[];
  @Input() selectedFlatIdx: number;
  @Output() selectFlatByIdx = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
  }


  selectFlat(id: number) {
    this.selectFlatByIdx.emit(id);
  }

  deselectFlat() {
    this.selectFlatByIdx.emit(undefined);
  }
}
