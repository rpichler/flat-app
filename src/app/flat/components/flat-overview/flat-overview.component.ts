import { Flat } from './../../flat.model';
import { AppState } from './../../../reducers/index';
import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { selectAllFlats, selectselectedFlat } from '../../ngrx/flat.selector';
import { FlatsRequested, SelectFlat } from '../../ngrx/flat.actions';

@Component({
  selector: 'app-flat-overview',
  templateUrl: './flat-overview.component.html',
  styleUrls: ['./flat-overview.component.scss']
})
export class FlatOverviewComponent implements OnInit {

  flats$: Observable<Flat[]>;
  selectedFlat$: Observable<number>;

  constructor(private _store: Store<AppState>) { }

  ngOnInit() {
    this._store.dispatch(new FlatsRequested());
    this.flats$ = this._store.pipe(
      select(selectAllFlats)
    );
    this.selectedFlat$ = this._store.pipe(select(selectselectedFlat));
  }

  changeSelectedFlat(id: number) {
    this._store.dispatch(new SelectFlat({id}));
  }
}
