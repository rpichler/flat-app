import { Flat } from './../../flat.model';
import { Observable, Subscription } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { AppState } from './../../../reducers/index';
import { Component, OnInit, OnDestroy, Input, EventEmitter, Output } from '@angular/core';
import { tileLayer, latLng, marker, icon, IconOptions, Layer } from 'leaflet';
import { selectAllFlats, selectselectedFlat } from '../../ngrx/flat.selector';
import { combineLatest, tap } from 'rxjs/operators';

@Component({
  selector: 'app-flat-map',
  templateUrl: './flat-map.component.html',
  styleUrls: ['./flat-map.component.scss']
})
export class FlatMapComponent implements OnInit, OnDestroy {

  @Input() selectedFlatId: number;
  @Output() changeSelectedFlat = new EventEmitter<number>();

  layers: Layer[];
  flatSubscription: Subscription;

  options = {
    layers: [
      tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 20, attribution: 'OpenStreetmap' })
    ],
    zoom: 14,
    center: latLng(34.0559, -118.2351)
  };

  readonly baseIcon: IconOptions = {
    iconSize: [27, 38],
    iconAnchor: [14, 19 ],
    iconUrl: '../../../../assets/marker-images/marker-base.png'
  };

  readonly highlightIcon: IconOptions = {
    iconSize: [27, 38],
    iconAnchor: [14, 19],
    iconUrl: '../../../../assets/marker-images/marker-highlight.png'
  };

  constructor(private _store: Store<AppState>) { }

  ngOnInit() {
    this.flatSubscription = this._store.pipe(
      select(selectAllFlats),
      combineLatest(this._store.pipe(
        select(selectselectedFlat)
      )),
      // tap(
      //   ([flats, selected]) => {
      //     console.log(flats, selected);
      //     return [flats, selected];
      //   }
      // )

    ).subscribe(
      ([data, selected]) => {
        if (data) {
          this.layers = [];
          for (const flat of data) {
            if (flat.id === selected) {
              this.layers.push(
                marker([flat.lat, flat.lng], { icon: icon(this.highlightIcon) })
              );
              // console.log('a', this.layers[0]);
              } else {
              this.layers.push(
                marker([flat.lat, flat.lng], {icon: icon(this.baseIcon)})
              );
            }
          }
          // console.log(this.layers[0]);
        }
      }
    );
  }

  ngOnDestroy() {
    this.flatSubscription.unsubscribe();
  }

  changeSelectedFlatId(id: number) {
    this.changeSelectedFlat.emit(id);
  }

}
