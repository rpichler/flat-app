import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FlatRoutingModule } from './flat-routing.module';
import { FlatOverviewComponent } from './components/flat-overview/flat-overview.component';
import { StoreModule } from '@ngrx/store';
import * as fromFlat from './ngrx/flat.reducer';
import { FlatListComponent } from './components/flat-list/flat-list.component';
import { FlatMapComponent } from './components/flat-map/flat-map.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCardModule } from '@angular/material';
import { EffectsModule } from '@ngrx/effects';
import { FlatEffects } from './ngrx/flat.effects';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';

@NgModule({
  declarations: [FlatOverviewComponent, FlatListComponent, FlatMapComponent],
  imports: [
    CommonModule,
    FlatRoutingModule,
    FlexLayoutModule,
    LeafletModule,
    MatCardModule,
    StoreModule.forFeature('flat', fromFlat.flatReducer, { metaReducers: fromFlat.metaReducers }),
    EffectsModule.forFeature([FlatEffects]),
  ]
})
export class FlatModule { }
