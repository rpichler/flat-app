import { TestBed, inject } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { FlatEffects } from './flat.effects';

describe('FlatEffects', () => {
  let _actions$: Observable<any>;
  let effects: FlatEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        FlatEffects,
        provideMockActions(() => _actions$)
      ]
    });

    effects = TestBed.get(FlatEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
