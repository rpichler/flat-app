import { environment } from '../../../environments/environment';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { Flat } from '../flat.model';
import { FlatActions, FlatActionTypes } from './flat.actions';
import { MetaReducer } from '@ngrx/store';

export interface FlatState extends EntityState<Flat> {
  selectedFlat: number;
}

export const adapter: EntityAdapter<Flat> = createEntityAdapter<Flat>();

export const initialState: FlatState = adapter.getInitialState({
  selectedFlat: undefined
});

export function flatReducer(
  state = initialState,
  action: FlatActions
): FlatState {
  switch (action.type) {
    // case FlatActionTypes.AddFlat: {
    //   return adapter.addOne(action.payload.flat, state);
    // }

    // case FlatActionTypes.UpsertFlat: {
    //   return adapter.upsertOne(action.payload.flat, state);
    // }

    // case FlatActionTypes.AddFlats: {
    //   return adapter.addMany(action.payload.flats, state);
    // }

    // case FlatActionTypes.UpsertFlats: {
    //   return adapter.upsertMany(action.payload.flats, state);
    // }

    case FlatActionTypes.UpdateFlat: {
      return adapter.updateOne(action.payload.flat, state);
    }

    case FlatActionTypes.UpdateFlats: {
      return adapter.updateMany(action.payload.flats, state);
    }

    // case FlatActionTypes.DeleteFlat: {
    //   return adapter.removeOne(action.payload.id, state);
    // }

    // case FlatActionTypes.DeleteFlats: {
    //   return adapter.removeMany(action.payload.ids, state);
    // }

    case FlatActionTypes.LoadFlats: {
      return adapter.addAll(action.payload.flats, state);
    }

    // case FlatActionTypes.ClearFlats: {
    //   return adapter.removeAll(state);
    // }

    case FlatActionTypes.SelectFlat: {
      return {...state, selectedFlat : action.payload.id };
    }

    case FlatActionTypes.DeselectFlat: {
      return {...state, selectedFlat: undefined};
    }

    default: {
      return state;
    }
  }
}

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors();

export const metaReducers: MetaReducer<FlatState>[] = !environment.production ? [] : [];
