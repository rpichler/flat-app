import { FlatPageQuery, FlatPageFilter } from './flat.actions';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { FlatState } from './flat.reducer';
import * as fromFlat from './flat.reducer';

export const selectFlatState = createFeatureSelector<FlatState>('flat');

export const selectAllFlats = createSelector(
  selectFlatState,
  fromFlat.selectAll,
);

export const selectFlatsPage = (page: FlatPageQuery, filter: FlatPageFilter) => createSelector(
  selectAllFlats,
  allFlats => {
    const startIndex = page.pageIndex * page.pageSize;
    const endIndex = startIndex + page.pageSize;

    return allFlats
      .filter((flat) => {
        if (
          (!filter.maxPrice || filter.maxPrice > Number(flat.price.amount))
        ) {
          return flat;
        }
      })
      .slice(startIndex, endIndex);
  }
);

export const selectselectedFlat = createSelector(
  selectFlatState,
  (flatState) => flatState.selectedFlat
);
