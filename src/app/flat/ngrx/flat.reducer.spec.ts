import { flatReducer, initialState } from './flat.reducer';

describe('Flat Reducer', () => {
  describe('unknown action', () => {
    it('should return the initial state', () => {
      const action = {} as any;

      const result = flatReducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });
});
