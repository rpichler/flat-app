import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { Flat } from '../flat.model';

export interface LatLngBounds {
  minLat: number;
  maxLat: number;
  minLng: number;
  maxLng: number;
}

export interface FlatPageQuery {
  pageIndex: number;
  pageSize: number;
}

export interface FlatPageFilter {
  maxPrice: number;
  latlngBounds: LatLngBounds;
}

export enum FlatActionTypes {
  FlatsRequested = '[Flat Overview Page] Flats Requested',
  LoadFlats = '[Flat Service] Flats Loaded',
  // AddFlat = '[Flat] Add Flat',
  // UpsertFlat = '[Flat] Upsert Flat',
  // AddFlats = '[Flat] Add Flats',
  // UpsertFlats = '[Flat] Upsert Flats',
  UpdateFlat = '[Flat] Update Flat',
  UpdateFlats = '[Flat] Update Flats',
  // DeleteFlat = '[Flat] Delete Flat',
  // DeleteFlats = '[Flat] Delete Flats',
  // ClearFlats = '[Flat] Clear Flats'
  SelectFlat = '[Flat Overview Page] Flat Selected',
  DeselectFlat = '[Flat Overview Page] Flat Deselected',
}

export class FlatsRequested implements Action {
  readonly type = FlatActionTypes.FlatsRequested;

  constructor() {}
}

export class LoadFlats implements Action {
  readonly type = FlatActionTypes.LoadFlats;
  constructor(public payload: { flats: Flat[] }) {}

}

// export class AddFlat implements Action {
//   readonly type = FlatActionTypes.AddFlat;

//   constructor(public payload: { flat: Flat }) {}
// }

// export class UpsertFlat implements Action {
//   readonly type = FlatActionTypes.UpsertFlat;

//   constructor(public payload: { flat: Flat }) {}
// }

// export class AddFlats implements Action {
//   readonly type = FlatActionTypes.AddFlats;

//   constructor(public payload: { flats: Flat[] }) {}
// }

// export class UpsertFlats implements Action {
//   readonly type = FlatActionTypes.UpsertFlats;

//   constructor(public payload: { flats: Flat[] }) {}
// }

export class UpdateFlat implements Action {
  readonly type = FlatActionTypes.UpdateFlat;

  constructor(public payload: { flat: Update<Flat> }) {}
}

export class UpdateFlats implements Action {
  readonly type = FlatActionTypes.UpdateFlats;

  constructor(public payload: { flats: Update<Flat>[] }) {}
}

// export class DeleteFlat implements Action {
//   readonly type = FlatActionTypes.DeleteFlat;

//   constructor(public payload: { id: string }) {}
// }

// export class DeleteFlats implements Action {
//   readonly type = FlatActionTypes.DeleteFlats;

//   constructor(public payload: { ids: string[] }) {}
// }

// export class ClearFlats implements Action {
//   readonly type = FlatActionTypes.ClearFlats;
// }

export class SelectFlat implements Action {
  readonly type = FlatActionTypes.SelectFlat;

  constructor(public payload: {id: number}) {}
}

export class DeselectFlat implements Action {
  readonly type = FlatActionTypes.DeselectFlat;
}

export type FlatActions =
 FlatsRequested
 | LoadFlats
//  | AddFlat
//  | UpsertFlat
//  | AddFlats
//  | UpsertFlats
 | UpdateFlat
 | UpdateFlats
//  | DeleteFlat
//  | DeleteFlats
//  | ClearFlats
 | SelectFlat
 | DeselectFlat
;
