import { AppState } from '../../reducers/index';
import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { FlatsRequested, FlatActionTypes, LoadFlats } from './flat.actions';
import { Store, select } from '@ngrx/store';
import { withLatestFrom, filter, mergeMap, map } from 'rxjs/operators';
import { selectAllFlats } from './flat.selector';
import { FlatService } from '../services/flat.service';


@Injectable()
export class FlatEffects {

  @Effect()
  loadFlats$ = this._actions$.pipe(
    ofType<FlatsRequested>(FlatActionTypes.FlatsRequested),
    withLatestFrom(this._store.pipe(select(selectAllFlats))),
    filter(([actions, allFlats]) => {
      return allFlats.length === 0;
    }),
    mergeMap(() => this._flatService.fetchFlatData()),
    map(flats => new LoadFlats({flats}))
  );

  constructor(
    private _actions$: Actions,
    private _store: Store<AppState>,
    private _flatService: FlatService,
    ) {}
}
