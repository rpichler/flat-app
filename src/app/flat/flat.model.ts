import { Money } from './../model/Money.model';
export interface Flat {
  id: number;
  lat: number;
  lng: number;
  streetAddress: string;
  zip: number;
  price: Money;
  picturePath: string;
  bedRoom: number;
  bathRoom: number;
  sizeFt: number;
}
